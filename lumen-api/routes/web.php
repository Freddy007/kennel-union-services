<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return ["name" => "UPLOAD Service", "port" => "8003"];
});

$router->group(['prefix' => 'api'], function () use ($router) {
        // Uploads
    $router->get('uploads', 'UploadsController@index');
    $router->post('uploads', 'UploadsController@create');
    $router->post('uploads/file/new', 'UploadsController@uploadOnlyFile');
    $router->get('uploads/{id}', 'UploadsController@show');
    $router->post('uploads/{id}', 'UploadsController@delete');

        // Search
    $router->get('search',"SearchController@search");
    $router->get('search/index',"SearchController@indexSearch");
});



