<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileSizeToFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->float("file_size")->default(0.0)->after("type");
            $table->string("cover_art_url")->nullable()->after("file_size");
            $table->float("cover_art_size")->nullable()->after("cover_art_url");
            $table->string("cover_art_extension")->nullable()->after("cover_art_size");
            $table->string("isbn")->nullable()->after("file_size");
            $table->string("year")->nullable()->after("isbn");
            $table->bigInteger("class_id")->nullable()->after("year");
            $table->bigInteger("subject_id")->nullable()->after("class_id");
            $table->bigInteger("user_id")->nullable()->after("subject_id");
            $table->integer("duration")->default(0)->nullable();
            $table->integer("page_count")->default(0)->nullable();
            $table->json("meta_info")->nullable();
            $table->json("user_object")->nullable();
            $table->json("subject_object")->nullable();
            $table->integer("downloads")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn(["meta_info","file_size","isbn","year","class_id", "subject_id",
                "user_id", "cover_art_url","cover_art_size","cover_art_extension", "page_count","downloads"]);
        });
    }
}
