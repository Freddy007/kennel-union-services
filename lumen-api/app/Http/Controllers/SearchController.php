<?php

namespace App\Http\Controllers;

use App\Models\Dog;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class SearchController  extends Controller
{
    use ConsumeExternalService;

    public function __construct()
    {
    }

    public function search(Request $request){
        $search = Dog::search($request->query("q"))->take(20)->get();

        return $this->response($search,200);

    }



}
