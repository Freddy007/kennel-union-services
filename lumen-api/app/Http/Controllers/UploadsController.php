<?php

namespace App\Http\Controllers;

use App\Jobs\UploadJob;
use App\Models\Dog;
use App\Traits\ConsumeExternalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UploadsController  extends Controller
{
    use ConsumeExternalService;

    private $storage;
    private $s3_url;

    public function __construct()
    {
        $this->s3_url = "https://rs-file-service.s3.af-south-1.amazonaws.com/";
        $this->storage = Storage::disk("s3");
    }

    public function index(Request $request){


        if (!($request->has("service_id"))){
            return $this->response(["message" => "service_id parameter is required!"],400);
        }


        $service_id = $request->query("service_id");
        $query = $request->query("field");
        $value = $request->query("value");

        $builder = Dog::whereServiceId($service_id);

        switch ($query){
            case "extension":
                ($builder->whereExtension($value));
                break;

            case "type":
                ($builder->whereType($value));
                break;

            case "tags":
                $builder->whereJsonContains("tags", $value);
                break;

            default:
                $builder->get();
                break;
        }

        return $builder;
    }

    public function create(Request $request)
    {
        $this->validateRequest($request);
        $upload_types = $this->getUploadTypes();

        if (!array_key_exists($request->type,$upload_types)){
            return response()->json(["message" => "Resource type not valid! Choose from books, audiobooks,articles,videos"],400);
        }

        try {

            DB::beginTransaction();
            $file_model = $this->saveToFileModel($request);

            $levels = explode(",",$request->get("levels"));

            if ($file_model) {
                foreach ($levels as $level) {
                    DB::table("files_levels")->insert([
                        "level_id" => $level,
                        "file_id" => $file_model->id
                    ]);
                }

                $job = new UploadJob($file_model->id,$file_model->user_id,$file_model->subject_id);
                $this->dispatch($job);
            }

            DB::commit();

        }catch (\Exception $exception){
            return response()->json(["message" => $exception->getMessage()]);
        }

        return response()->json(["message" => "success"],200);
    }

    public function update(Request $request,$id){
        Dog::find($id)->update([
            "name" => $request->name,
            "service_id" => $request->service_id,
            "storage" => "s3",
            "file_url" => $request->book_url,
            "file_size" => $request->book_size,
            "extension" => $request->book_extension,
            "cover_art_url" => $request->cover_art_url,
            "cover_art_size" => $request->cover_art_size,
            "cover_art_extension" => $request->cover_art_extension,
            "isbn" => $request->isbn,
            "year" => $request->year,
            "class_id" => $request->class_id,
            "subject_id" => $request->subject_id,
            "user_id" => $request->user_id,
            "page_count" => $request->page_count,
            "meta_info" => json_encode($request->meta_info),
            "type" => $request->type,
            "tags" => json_encode([$request->tags])
        ]);
    }


    public function uploadOnlyFile(Request $request){

        $this->validate($request,["file"=>"required|file","resource_type" => "required","image_type"=>"required"]);

        if (!array_key_exists($request->resource_type,$this->getUploadTypes())){
            return response()->json(["message" => "Resource type not valid! Choose from books,audiobooks,articles,videos"],400);
        }

        $mimeType = $request->get('resource_type');
        $imageType = $request->get('image_type');
        $file = $request->file('file');
        $file_name = str_replace([" ",",","(",")"],"-",($file->getClientOriginalName()));
        $path = $this->getPath($mimeType,$imageType);
        $file_url = $this->s3_url.$path.$file_name;
        $imageSize = $file->getSize();
        $file_size = number_format($imageSize / 1048576,2);
        $this->uploadFile($path, $file_name, $request);
        return $this->response(
            [
                "url" => $file_url,
                "extension" => $file->getClientOriginalExtension(),
                "size" => $file_size,
            ],200);
    }

    public function show(Request $request, $id){
        $file = Dog::find($id);

        if ($file){
            return response()->json(["message" => "success","data" => $file]);
        }

        return response()->json(["message" => "empty", "data" => null]);
    }

    public function delete(Request $request, $id){
        $file_model = Dog::find($id);
        if ($file_model)
            $file_model->delete();
            $file_url = $request->file_url;
            $this->storage->delete($file_url);

        return $this->response($file_url,200);
    }

    /**
     * @param $mimeType
     * @return string
     */
    public function getPath($mimeType, $image_type): string
    {
        $file_dest = $image_type == "cover-arts" ? "cover-arts":"files";

        $books_path = "gkb/books/$file_dest/";
        $audio_books_path="gkb/audiobooks/$file_dest/";
        $videos_path="gkb/videos/$file_dest/";
        $articles_path="gkb/articles/$file_dest/";

        $path="";
        switch ($mimeType) {
            case "books":
                $path = $books_path;
                break;

            case "audiobooks":
                $path = $audio_books_path;
                break;

            case "articles":
                $path = $articles_path;
                break;

            case "videos":
                $path = $videos_path;
                break;
        }
        return $path;
    }

    /**
     * @param string $path
     * @param string $file_name
     * @param Request $request
     */
    public function uploadFile(string $path, string $file_name, Request $request): string
    {
        return $this->storage->put($path.$file_name,
            file_get_contents($request->file('file')->getRealPath()), [
                'visibility' => 'public'
            ]);
    }

    public function getUpload($id){
        $file_model = Dog::find($id);

        return response()->json(["data" => $file_model]);
    }

    /**
     * @param Request $request
     */
    public function saveToFileModel(Request $request)
    {
        $fileModel = Dog::create([
            "name" => $request->name,
            "service_id" => $request->service_id,
            "storage" => "s3",
            "file_url" => $request->book_url,
            "file_size" => $request->book_size,
            "extension" => $request->book_extension,
            "cover_art_url" => $request->cover_art_url,
            "cover_art_size" => $request->cover_art_size,
            "cover_art_extension" => $request->cover_art_extension,
            "isbn" => $request->isbn,
            "year" => $request->year,
            "class_id" => $request->class_id,
            "subject_id" => $request->subject_id,
            "user_id" => $request->user_id,
            "page_count" => $request->page_count,
            "meta_info" => json_encode($request->meta_info),
            "type" => $request->type,
            "tags" => json_encode([$request->tags]),
            "downloads" => $request->has("downloads")? rand(0,100) : 0
        ]);

        return $fileModel;
    }

    private function indexModel(){
        Artisan::call("scout:import",["model" => "App\\Models\\FileModel" ]);
    }

    /**
     * @return string[]
     */
    public function getUploadTypes(): array
    {
        $upload_types = [
            "books"      => "books",
            "articles"   => "articles",
            "audiobooks" => "audiobooks",
            "videos"     => "videos"
        ];
        return $upload_types;
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateRequest(Request $request): void
    {
        $this->validate($request, [
            'name'           => 'required|unique:files',
            'type'           => 'required',
            'book_url'       => 'required',
            'book_size'      => 'required',
            'cover_art_url'  => 'required',
            'cover_art_size' => 'required',
            'user_id'        => 'required',
            'subject_id'     => 'required',
            'service_id'     => 'required',
            "levels"         => 'required'
        ]);
    }
}
