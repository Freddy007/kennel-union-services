<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Artisan;

class IndexJob extends Job implements ShouldQueue
{
    public function __construct()
    {

    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        Artisan::call("scout:flush",["model" => "App\\Models\\FileModel" ]);
//        Artisan::call("scout:import",["model" => "App\\Models\\FileModel" ]);
    }


}
