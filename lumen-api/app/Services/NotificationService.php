<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class NotificationService
{
    use ConsumeExternalService;

    private  $base_url;
    private  $secret;
    private  $notify_url;

    public function __construct()
    {
        $this->base_url = config("services.notifications.base_url");
        $this->secret = config("services.notifications.secret");
        $this->notify_url = config("services.notifications.endpoints.notify_url");
    }

    public function notify($options,$headers){
        return $this->performRequest($this->base_url,$this->secret,"POST",$this->notify_url,$options,$headers);
    }

}
