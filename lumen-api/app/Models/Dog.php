<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Scout\Searchable;

class Dog extends Model
{
    use HasFactory,Searchable;
    protected $table = "dogs";

    protected $guarded = [];

    protected $casts = [
        "id" => "string"
    ];

    public function toSearchableArray()
    {
        $array = $this->only('id', 'name', 'registration_number','deleted_at');
        return ($array);
    }
}
