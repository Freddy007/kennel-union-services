<?php

use Illuminate\Support\Str;

return [

    'payments'=> [
        "base_url"           => "http://payment:8000",
        "secret"             => env("PAYMENT_SERVICE_SECRET"),
        "endpoints" => [
            "make_payment_url"   => "/api/payments/transactions",
            "verify_payment_url" => "/api/payments/verify-payment/",
            "submit_otp"         => "/api/payments/transactions/otp"
            ]
    ],


    'notifications' => [
        'base_url' => "http://notification:8001",
        "secret"   => env("NOTIFICATION_SERVICE_SECRET"),
        "endpoints" => [
            "notify_url" => "/api/notify"
        ]
    ],
//
    'search' => [
        'base_url' => "http://search:8002",
        "secret"   => env("NOTIFICATION_SERVICE_SECRET")
    ],

    'users' => [
        'base_url' => "http://nginx:80",
        "endpoints" => [ 'users_url' => "/api/users/",
        ],
        "secret"   => env("USER_SERVICE_SECRET")
    ],

    'subjects' => [
        'base_url' => "http://curriculums:8006",
        "endpoints" => ['subjects_url' => "/api/subjects/"],
        "secret"   => env("SUBJECT_SERVICE_SECRET")
    ]
];
