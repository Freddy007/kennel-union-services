@extends('version2.layouts.admin_layout')

@section('styles')

@endsection

@section('scripts')

    <script src="{{asset('kug_version2/assets/global/scripts/select2.min.js')}}" type="text/javascript"></script>

@endsection


@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>Attendee dog
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{url('/version2/events')}}">Events</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                        {{--<a href="#">Tables</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>attendee dog</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">

                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            {{--<i class="icon-equalizer font-red-sunglo"></i>--}}
                                            <span class="caption-subject font-red-sunglo bold uppercase"></span>
                                            {{--<span class="caption-helper">{{$dog->name}}</span>--}}
                                        </div>
                                        <div class="actions">
                                            <div class="portlet-input input-inline input-small">
                                                <div class="input-icon right">
                                                    {{--<i class="icon-magnifier"></i>--}}
                                                    {{--<input type="text" class="form-control input-circle" placeholder="search..."> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">

                                    </div>
                                </div>


                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection
