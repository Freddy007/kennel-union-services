<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certified Pedigree</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Charm&display=swap" rel="stylesheet">
    <style>
        /* Basic Reset */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
            background-color: #f5f5f5;
        }

        /* Main container styling to fit custom PDF dimensions */
        .container {
            display: flex;
            flex-direction: column;
            height: 1500px;
            width: 2000px;
            padding: 30px;
            box-sizing: border-box;
            justify-content: space-between;
        }

        /* Flag bars */
        .flag { height: 20px; }
        .flag-red { background-color: red; }
        .flag-yellow { background-color: yellow; }
        .flag-green { background-color: green; }

        /* Center content */
        .content {
            display: flex;
            flex-direction: column;
            flex: 1;
            justify-content: center;
        }

        /* Header and text alignment */
        .header h1 { font-size: 3em; color: #333; margin: 0; }
        .header h2 { font-size: 2.5em; color: #666; margin: 0; }

        /* Info and signature sections */
        .info-section, .signature-section {
            padding: 15px;
            background-color: #e8e8e8;
            border-radius: 10px;
            margin-bottom: 20px;
        }

        .info-table, .pedigree-table {
            width: 100%;
            border-collapse: collapse;
            font-size: 1.2em;
            margin-top: 15px;
        }

        .info-table td, .pedigree-table th, .pedigree-table td {
            padding: 15px;
            border: 1px solid #333;
            text-align: center;
        }

        .signature-section p {
            font-size: 1.5em;
            margin-top: 20px;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="flag flag-red"></div>
    <div class="flag flag-yellow"></div>
    <div class="flag flag-green"></div>

    <div class="content">
        <table class="table">
            <tr>
                <td><p class="text-muted small">info@kennelunionofghana.com</p><p class="text-danger font-weight-bold">EXPORT PEDIGREE</p></td>
                <td><img src="https://bucket-uupw4h.s3.eu-west-1.amazonaws.com/FCI_logo.svg.png" width="150" /></td>
                <td class="header text-center"><h1>KENNEL UNION OF GHANA</h1><h2>CERTIFIED PEDIGREE</h2></td>
                <td><img src="https://bucket-uupw4h.s3.eu-west-1.amazonaws.com/KUG_Logo%20Mark.png" width="150" /></td>
                <td><p class="small">https://www.kennelunionofghana.com</p><p>EX 0000001</p></td>
            </tr>
        </table>

        <div class="info-section" style="background-color: #CCFFCC;">
            <table class="info-table">
                <tr>
                    <td class="info-label">Name</td>
                    <td>{{ strtoupper($dog->name) }}</td>
                    <td class="info-label">Pedigree No</td>
                    <td>[Pedigree No]</td>
                    <td class="info-label">Microchip</td>
                    <td>{{ $dog->microchip_number }}</td>
                </tr>
                <tr>
                    <td class="info-label">Breed</td>
                    <td>{{ strtoupper($dog->breed) }}</td>
                    <td class="info-label">Sex</td>
                    <td>{{ strtoupper($dog->sex) }}</td>
                    <td class="info-label">Color</td>
                    <td>{{ strtoupper($dog->color) }}</td>
                </tr>
                <tr>
                    <td class="info-label">Date of Birth</td>
                    <td>{{ strtoupper($dog->dob) }}</td>
                    <td class="info-label">Coat</td>
                    <td>{{ strtoupper($dog->coat) }}</td>
                    <td class="info-label">Size</td>
                    <td>[Size]</td>
                </tr>
                <tr>
                    <td class="info-label">Breeder</td>
                    <td>{{ strtoupper($dog->breeder_name) }}</td>
                    <td class="info-label">Owner Address</td>
                    <td colspan="3">{{ $address }}</td>
                </tr>
            </table>
        </div>

        <div class="section">
            <table class="pedigree-table">
                <tr><td rowspan="4">[Parent 1]</td><td rowspan="2">[Grandparent 1]</td><td>[Great-Grandparent 1]</td></tr>
                <tr><td>[Great-Grandparent 2]</td></tr>
                <tr><td rowspan="2">[Grandparent 2]</td><td>[Great-Grandparent 3]</td></tr>
                <tr><td>[Great-Grandparent 4]</td></tr>
                <tr><td rowspan="4">[Parent 2]</td><td rowspan="2">[Grandparent 3]</td><td>[Great-Grandparent 5]</td></tr>
                <tr><td>[Great-Grandparent 6]</td></tr>
                <tr><td rowspan="2">[Grandparent 4]</td><td>[Great-Grandparent 7]</td></tr>
                <tr><td>[Great-Grandparent 8]</td></tr>
            </table>
        </div>

        <div class="signature-section">
            <table>
                <tr>
                    <td>Transfer Date:</td>
                    <td>____________</td>
                    <td>New Owner Name:</td>
                    <td>____________</td>
                    <td>New Owner Address:</td>
                    <td>____________</td>
                    <td>Signature:</td>
                    <td>____________</td>
                </tr>
            </table>
            <p>I, <strong>SAMIR MSAILEB</strong>, the guardian of the KUG stud book, certify that the above information is correct and reliable. <span>{{ date('d-m-Y') }}</span></p>
        </div>
    </div>

    <div class="flag flag-red"></div>
    <div class="flag flag-yellow"></div>
    <div class="flag flag-green"></div>
</div>
</body>
</html>
