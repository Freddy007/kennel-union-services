

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
    <title>Homepage &middot; MVP Ready</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--<meta name="description" content="">--}}
    {{--<meta name="author" content="">--}}
    {!! \Artesaos\SEOTools\Facades\SEOMeta::generate() !!}

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Google Font: Open Sans -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="../../bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">



    <!-- App CSS -->
    <link rel="stylesheet" href="css/mvpready-landing.css">
    <link href="../../bower_components/animate.css/animate.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="./css/custom.css"> -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
</head>

<body class="">

<div id="wrapper">

    <header class="navbar" role="banner">

        <div class="container">

            <div class="navbar-header">
                <a href="index-2.html" class="navbar-brand navbar-brand-img">
                    <img src="img/logo.png" alt="MVP Ready">
                </a>

                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-bars"></i>
                </button>
            </div> <!-- /.navbar-header -->


            <nav class="collapse navbar-collapse" role="navigation">

                <ul class="nav navbar-nav navbar-right mainnav-menu">
                    <li class="active">

                        <a href="index.html">
                            Home
                        </a>

                    </li>

                    <li class="dropdown ">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                            Pages
                            <i class="fa fa-caret-down navbar-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="page-team.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    Our Team
                                </a>
                            </li>

                            <li>
                                <a href="page-company.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    Our Company
                                </a>
                            </li>

                            <li>
                                <a href="page-tour.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    Product Tour
                                </a>
                            </li>

                            <li>
                                <a href="page-faq.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    FAQ
                                </a>
                            </li>

                            <li>
                                <a href="page-pricing-plans.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    Pricing Plans
                                </a>
                            </li>

                            <li>
                                <a href="page-pricing-table.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    Price Comparison
                                </a>
                            </li>

                            <li>
                                <a href="page-jobs.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    Job Openings
                                </a>
                            </li>

                            <li>
                                <a href="page-contact.html">
                                    <i class="fa fa-angle-double-right dropdown-icon"></i>
                                    Contact
                                </a>
                            </li>
                        </ul>

                    </li>

                    <li class="dropdown ">

                    </li>

                    <li class="dropdown ">

                    </li>

                    <li class="dropdown ">

                    </li>

                    <li class="dropdown ">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                            Blog
                            <i class="fa fa-caret-down navbar-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="page-blog.html">
                                    <i class="fa fa-comments-o dropdown-icon"></i>
                                    Blog Index
                                </a>
                            </li>

                            <li>
                                <a href="page-blog-single.html">
                                    <i class="fa fa-comments-o dropdown-icon"></i>
                                    Blog Single
                                </a>
                            </li>
                        </ul>

                    </li>

                    <li class="dropdown ">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                            Login
                            <i class="fa fa-caret-down navbar-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="account-login.html">
                                    <i class="fa fa-unlock dropdown-icon"></i>
                                    Login
                                </a>
                            </li>

                            <li>
                                <a href="account-login-social.html">
                                    <i class="fa fa-unlock dropdown-icon"></i>
                                    Login Social
                                </a>
                            </li>

                            <li>
                                <a href="account-forgot.html">
                                    <i class="fa fa-unlock dropdown-icon"></i>
                                    Forgot Password
                                </a>
                            </li>

                            <li>
                                <a href="account-signup.html">
                                    <i class="fa fa-star dropdown-icon"></i>
                                    Signup
                                </a>
                            </li>

                        </ul>

                    </li>

                    <li class="dropdown">

                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                            More Templates
                            <i class="fa fa-caret-down navbar-caret"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="https://jumpstartthemes.com/demo/v/2.1.0/templates/admin/">
                                    <i class="fa fa-external-link dropdown-icon"></i>
                                    Admin Template
                                </a>
                            </li>
                            <li>
                                <a href="https://jumpstartthemes.com/demo/v/2.1.0/templates/launch/">
                                    <i class="fa fa-external-link dropdown-icon"></i>
                                    Launch Template
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </nav>

        </div> <!-- /.container -->

    </header>



    <div class="masthead">

        <!-- starts carousel -->

        <div id="masthead-carousel" class="carousel slide carousel-fade masthead-carousel">
            <div class="carousel-inner">

                <div class="item active" style="background-color: #354b5e;">

                    <br class="xs-30 sm-60">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-6 masthead-text animated fadeInDownBig">
                                <h4 class="masthead-title">Don't feel limited to color schemes provided.</h4>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi ut aliquip ex ea commodo consequat.
                                </p>

                                <br>

                                <div class="masthead-actions">
                                    <a href="page-tour.html" class="btn btn-transparent btn-jumbo">
                                        Learn More
                                    </a>

                                    <a href="account-signup.html" class="btn btn-primary btn-jumbo">
                                        Create an Account
                                    </a>

                                </div> <!-- /.masthead-actions -->

                            </div> <!-- /.masthead-text -->

                            <br class="xs-30 md-0">

                            <div class="col-md-6 masthead-img animated fadeInUpBig">
                                <img src="img/masthead/mobile.png" alt="slide2" class="img-responsive" />
                            </div> <!-- /.masthead-img -->

                        </div> <!-- /.row -->

                    </div> <!-- /.container -->

                </div> <!-- /.item -->

                <div class="item" style="height: 775px; background-color: #354b5e; background-image: url(../../global/img/bg/shattered.png);">

                    <br class="xs-30 sm-60">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-10 col-md-offset-1 masthead-text text-center animated fadeIn">
                                <h2 class="masthead-title">Create an Awesome, Engaging Landing Page for Your Next Project!</h2>

                                <div class="masthead-actions">
                                    <a href="page-tour.html" class="btn btn-transparent btn-jumbo">
                                        Take a Tour
                                    </a>

                                    <a href="account-signup.html" class="btn btn-primary btn-jumbo">
                                        Get Started Today
                                    </a>

                                </div> <!-- /.masthead-actions -->


                                <br class="xs-50">

                            </div> <!-- /.masthead-text -->

                            <div class="col-md-12 masthead-img animated fadeInUpBig">
                                <img src="img/masthead/browser.png" alt="slide2" class="img-responsive" />
                            </div> <!-- /.masthead-img -->

                        </div> <!-- /.row -->

                    </div> <!-- /.container -->

                </div> <!-- /.item -->

                <div class="item  " style="background-color: #B74444; background-image: url(../../global/img/bg/bright-squares.png);">

                    <br class="xs-30 sm-60">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-6 masthead-text animated fadeInLeftBig">
                                <h4 class="masthead-title">Don't feel limited to color schemes provided.</h4>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi ut aliquip ex ea commodo consequat.
                                </p>

                                <br>

                                <div class="masthead-actions">
                                    <a href="page-tour.html" class="btn btn-transparent btn-jumbo">
                                        Learn More
                                    </a>
                                </div> <!-- /.masthead-actions -->
                            </div> <!-- /.masthead-text -->

                            <br class="xs-30 md-0">

                            <div class="col-md-6 masthead-img animated fadeInRightBig">
                                <form class="form masthead-form well">

                                    <h3 class="text-center">Start Your Free Trial!</h3>

                                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, earum.</p>

                                    <br>

                                    <div class="form-group">
                                        <!-- <label class="" for="full_name">Full Name</label> -->
                                        <input type="text" id="full_name" placeholder="Your Name" class="form-control">
                                    </div> <!-- /.form-group -->

                                    <div class="form-group">
                                        <!-- <label class="" for="email_address">Email Address</label> -->
                                        <input type="text" id="email_address" placeholder="Your Email" class="form-control">
                                    </div> <!-- /.form-group -->

                                    <div class="form-group">
                                        <!-- <label class="" for="organization">Organization</label> -->
                                        <input type="text" id="organization" placeholder="Organization Name" class="form-control">
                                    </div> <!-- /.form-group -->

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-secondary btn-jumbo btn-block">Get Started Today!</button>
                                    </div> <!-- /.form-group -->

                                </form>

                                <br class="xs-80">
                            </div> <!-- /.masthead-img -->

                        </div> <!-- /.row -->

                    </div> <!-- /.container -->

                </div> <!-- /.item -->

                <div class="item " style="background-color: #354b5e; background-image: url(../../global/img/bg/dark-mosaic.png);">

                    <br class="xs-30 sm-60">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-6 masthead-text animated fadeInDownBig">
                                <h4 class="masthead-title">Don't feel limited to color schemes provided.</h4>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi ut aliquip ex ea commodo consequat.
                                </p>

                                <br>

                                <div class="masthead-actions">
                                    <a href="page-tour.html" class="btn btn-transparent btn-jumbo">
                                        Learn More
                                    </a>

                                    <a href="account-signup.html" class="btn btn-primary btn-jumbo">
                                        Create an Account
                                    </a>
                                </div> <!-- /.masthead-actions -->

                            </div> <!-- /.masthead-text -->

                            <br class="xs-30 md-0">

                            <div class="col-md-6 masthead-img animated pulse">
                                <iframe src="https://player.vimeo.com/video/57175742?title=0&amp;byline=0&amp;portrait=0&amp;color=e2007a" style="width: 85%; height: 325px;"></iframe>

                                <br class="xs-80">

                            </div> <!-- /.masthead-img -->

                        </div> <!-- /.row -->

                    </div> <!-- /.container -->

                </div> <!-- /.item -->

            </div>  <!-- /.carousel-inner -->
            <!-- Carousel nav -->


            <div class="container">
                <div class="carousel-controls">
                    <a class="carousel-control left" href="#masthead-carousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#masthead-carousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>

        </div> <!-- /.masthead-carousel -->

    </div> <!-- /.masthead -->

@yield('content')


</div> <!-- /#wrapper -->

<footer class="footer">

    <div class="container">

        <div class="row">

            <div class="col-sm-3">

                <div class="heading-block">
                    <h4>MVP Ready</h4>
                </div> <!-- /.heading-block -->

                <p>Aliquam fringilla, sapien egetferas scelerisque placerat, lorem libero cursus lorem, sed sodales lorem libero eu sapien. Nunc mattis feugiat justo vel faucibus. Nulla consequat feugiat malesuada.</p>

                <p>Placerat, lorem libero cursus lorem, sed sodales lorem libero eu sapien</p>
            </div> <!-- /.col -->


            <div class="col-sm-3">

                <div class="heading-block">
                    <h4>Keep In Touch</h4>
                </div> <!-- /.heading-block -->

                <ul class="icons-list">
                    <li>
                        <i class="icon-li fa fa-home"></i>
                        123 Northwest Way <br>
                        Las Vegas, NV 89183
                    </li>

                    <li>
                        <i class="icon-li fa fa-phone"></i>
                        +1 702 123 4567
                    </li>

                    <li>
                        <i class="icon-li fa fa-envelope"></i>
                        <a href="mailto:info@mvpready.com">info@mvpready.com</a>
                    </li>
                    <li>
                        <i class="icon-li fa fa-map-marker"></i>
                        <a href="javascript:;">View Map</a>
                    </li>
                </ul>
            </div> <!-- /.col -->


            <div class="col-sm-3">

                <div class="heading-block">
                    <h4>Connect With Us</h4>
                </div> <!-- /.heading-block -->

                <ul class="icons-list">

                    <li>
                        <i class="icon-li fa fa-facebook"></i>
                        <a href="javascript:;">Facebook</a>
                    </li>

                    <li>
                        <i class="icon-li fa fa-twitter"></i>
                        <a href="javascript:;">Twitter</a>
                    </li>

                    <li>
                        <i class="icon-li fa fa-soundcloud"></i>
                        <a href="javascipt:;">Sound Cloud</a>
                    </li>

                    <li>
                        <i class="icon-li fa fa-google-plus"></i>
                        <a href="javascript:;">Google Plus</a>
                    </li>
                </ul>

            </div> <!-- /.col -->


            <div class="col-sm-3">

                <div class="heading-block">
                    <h4>Stay Updated</h4>
                </div> <!-- /.heading-block -->

                <p>Get emails about new theme launches &amp;  future updates.</p>

                <form action="https://jumpstartthemes.com/" class="form">

                    <div class="form-group">
                        <!-- <label>Email: <span class="required">*</span></label> -->
                        <input class="form-control" id="newsletter_email" name="newsletter_email" type="text" value="" required="" placeholder="Email Address">
                    </div> <!-- /.form-group -->

                    <div class="form-group">
                        <button class="btn btn-transparent">Subscribe Me</button>
                    </div> <!-- /.form-group -->

                </form>

            </div> <!-- /.col -->

        </div> <!-- /.row -->

    </div> <!-- /.container -->

</footer>

<footer class="copyright">
    <div class="container">

        <div class="row">

            <div class="col-sm-12">
                <p>Copyright &copy; 2013-15 <a href="javascript:;">Jumpstart Themes</a>.</p>
            </div> <!-- /.col -->

        </div> <!-- /.row -->

    </div>
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Core JS -->
<script src="../../bower_components/jquery/dist/jquery.js"></script>
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="../../../../../../preview.oklerthemes.com/porto/4.1.0/vendor/jquery.gmap/jquery.gmap.js"></script>


<!-- App JS -->
<script src="../../global/js/mvpready-core.js"></script>
<script src="../../global/js/mvpready-helpers.js"></script>
<script src="js/mvpready-landing.js"></script>

</body>
</html>