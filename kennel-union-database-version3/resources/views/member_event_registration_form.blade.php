@php

@endphp

<script>
    function memberOnChange(e) {
        const value = e.target.value
        const memberForm = document.querySelector("#member-form")
        const nonmemberForm = document.querySelector("#non-member-form")

        if(value.includes("yes") ) {
            nonmemberForm.style.display = 'block'
            memberForm.style.display = 'block'
        }else if(value.includes("no")) {
            nonmemberForm.style.display = 'block'
            memberForm.style.display = 'none'
        }
    }

    function memberNumberOnChange(e){
        const value = e.target.value;

        fetch(`/get-member/${value}`).then((res) => res.json())
            .then((res) => {
                console.log("res from member", res)
                    document.querySelector("#full_name").value = `${res.first_name} ${res.last_name}`
                    document.querySelector("#phone_number").value = `${res.phone}`
                    document.querySelector("#email").value = `${res.email}`
            })
    }

    function eventOnChange(e) {
        const value = e.target.value
        const memberForm = document.querySelector("#member-form")
        const nonmemberForm = document.querySelector("#non-member-form")
        const memberQuestion = document.querySelector("#member-question")

        if(!value || value.length === 0){
            memberQuestion.style.display = 'none'
            return
        }
        memberQuestion.style.display = 'block'
    }

    function payWithPaystack() {
        const event_id = document.querySelector("#event_id").value;
        const kug_id   = document.querySelector("#kug_id").value;
        const id       = document.querySelector("#id").value

        document.querySelector('#payment-button').disabled = true;

        fetch(`/events/${event_id}`)
            .then((response) => response.json())
            .then((res) => {
                console.log("Response from res", res)
                const event_fee             = res.event_fee
                const assessment_fee        = res.member_assessment_fee
                const non_assessment_fee    = res.non_member_assessment_fee
                const is_member             = document.querySelector("#is_member").value
                const fee                   = is_member === "yes" ? Number(assessment_fee) : Number(non_assessment_fee)
                const full_name             = document.querySelector("#full_name").value
                const phone_number          = document.querySelector("#phone_number").value
                const email                 = document.querySelector("#email").value
                const location              = document.querySelector("#location").value
                const category_of_interest  = document.querySelector("#category_of_interest").value
                const mobile_money_number   = document.querySelector("#mobile_money_number").value
                const mobile_network        = document.querySelector("#mobile_network").value
                const handler_name          = document.querySelector("#handler_name").value
                const kug_id                = document.querySelector("#kug_id").value
                const name                  = document.querySelector("#dog_name").value
                const breed_id              = document.querySelector("#breed_id").value
                const gender                = document.querySelector("#gender").value
                const dob                   = document.querySelector("#dob").value
                const total                 = Number(event_fee) + Number(fee)

                const handler = PaystackPop.setup({
                    key: 'pk_live_05b2c532ebe0fa48f2bd710ab07223e7a1a5d0c9',
                    email: document.querySelector('#email').value,
                    amount: total * 100,
                    // amount: total * 1,
                    currency: 'GHS',
                    ref: ''+Math.floor((Math.random() * 1000000000) + 1),
                    callback: function(response) {
                        const reference = response.reference;
                        // alert('Payment complete! Reference: ' + reference);
                        console.log("transaction reference ", response)
                        const transaction_id        = response.trxref
                        if(response.message === "Approved" && response.status === "success"){
                            (async () => {
                                const rawResponse = await fetch('/event-registration', {
                                    method: 'POST',
                                    headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify(
                                        {
                                            full_name,
                                            phone_number,
                                            email,
                                            location,
                                            id,
                                            category_of_interest,
                                            mobile_money_number,
                                            mobile_network,
                                            handler_name,
                                            kug_id,
                                            name,
                                            breed_id,
                                            gender,
                                            dob,
                                            event_id,
                                            transaction_id,
                                            amount: total
                                        })
                                });
                                const content = await rawResponse.json();
                                uploadImage(content.data.dog_id)
                                alert("Successfully registered for the event")
                                window.location.href="/"
                            })();
                            uploadImage()
                        }
                    },
                    onClose: function() {
                        alert('Transaction was not completed, window closed.');
                        document.querySelector('#payment-button').disabled = false;
                    },
                });
                handler.openIframe();
            })
    }

    function uploadImage(id) {
        const input = document.querySelector('input[type="file"]')
        const data = new FormData()
        data.append('image', input.files[0])
        data.append('id', id)

        fetch('/upload-image', {
            method: 'POST',
            body: data
        })
    }

</script>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light " id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Event Registration Form -
                                                    <span class="step-title"> Step 1 of 4 </span>
                                                </span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-cloud-upload"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-wrench"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-trash"></i>
                    </a>
                </div>
            </div>

            <div class="portlet-body form">
                <form action="{{url('version2/register-new-dog')}}" class="form-horizontal" id="submit_form" method="POST">
                    {{--<form action="{{url('admin/register-dog')}}" class="form-horizontal" id="submit_form" method="POST">--}}
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">

                                            <i class="fa fa-check"></i> Attendee Details </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Dog(s) Details </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab" class="step active">
                                        <span class="number"> 3 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Payment Details </span>
                                    </a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="#tab4" data-toggle="tab" class="step">--}}
{{--                                        <span class="number"> 4 </span>--}}
{{--                                        <span class="desc">--}}
{{--                                            <i class="fa fa-check"></i> Confirm Details </span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
                            </ul>

                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success"> </div>
                            </div>
                            <div class="tab-content">
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                <div class="tab-pane active" id="tab1">
                                    <h3 class="block">Attendee's details</h3>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Select event
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-md-4">
                                            <select class="form-control" id="event_id" required
                                                    onchange="eventOnChange(event)">
                                                <option value=""></option>
                                                @foreach($events as $event_object)
                                                    <option value="{{$event_object->id}}">{{$event_object->name}} - {{$event_object->event_date}}</option>
                                                @endforeach
                                            </select>

                                            <i><span class="help-block event_id">  </span></i>
                                        </div>
                                    </div>

                                    <div class="form-group" style="display: none" id="member-question">
                                        <label class="control-label col-md-3">Are you a member of KUG?
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-md-4">
                                            <select class="form-control" id="is_member" required
                                                    onchange="memberOnChange(event)">
                                                <option value=""></option>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                            <i><span class="help-block is_member">  </span></i>
                                        </div>
                                    </div>

                                    <div id="member-form" style="display: none">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Kug ID
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input onchange="memberNumberOnChange(event)" type="text" class="form-control name" name="kug_id" id="kug_id"/>
                                                <i><span class="help-block kug_id">  </span></i>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" id="id" value="{{\Webpatser\Uuid\Uuid::generate()->string}}" />
                                    <div id="non-member-form" style="display: none">
                                        <div class="form-group" >
                                            <label class="control-label col-md-3">Name
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control full_name" name="full_name" id="full_name" required/>
                                                <i><span class="help-block full_name">  </span></i>
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label class="control-label col-md-3">Phone number
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control phone_number" name="phone_number" id="phone_number" required/>
                                                <i><span class="help-block phone_number">  </span></i>
                                            </div>
                                        </div>


                                        <div class="form-group" >
                                            <label class="control-label col-md-3">Email
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="email" class="form-control email" name="email" id="email" required/>
                                                <i><span class="help-block email">  </span></i>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Location
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control location" name="location" id="location"/>
                                                <i><span class="help-block location">  </span></i>
                                            </div>
                                        </div>

                                    </div>



                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name of Handler
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control handler_name" name="handler_name" id="handler_name" required/>
                                            <i><span class="help-block handler_name">  </span></i>
                                        </div>
                                </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Category of Interest
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="category_of_interest" id="category_of_interest" required>
                                                <option value="main-show">Main Show</option>
                                                <option value="assessment">Assessment</option>
                                            </select>
{{--                                            <input type="text" class="form-control name" name="name_of_handler" id="name_of_handler"/>--}}
                                            <i><span class="help-block name">  </span></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="tab-pane" id="tab2">
                                    <h3 class="block">Dog's details</h3>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name of dog
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="dog_name" id="dog_name" required />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Picture--}}
{{--                                            <span class="required"> * </span>--}}
{{--                                        </label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <input type="file" class="form-control" name="picture" id="file" required />--}}
{{--                                            <span class="help-block"> </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Breed
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-md-4">
                                            <select class="form-control" name="breed_id" id="breed_id" required>
                                                @foreach ($breeds as $brd)
                                                    <option value="{{$brd->id}}">{{strtolower($brd->name)}}</option>
                                            @endforeach
                                            </select>
                                    </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Gender
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                           <select class="form-control" name="gender" id="gender" required>
                                               <option value="male">Male</option>
                                               <option value="female">Female</option>
                                           </select>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date of birth
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" id="dob" name="dob" required />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Performance titles--}}
{{--                                            <span class="required">  </span>--}}
{{--                                        </label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <input type="text" class="form-control" name="performance_titles" />--}}
{{--                                            <span class="help-block"> </span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Image
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="file" required class="form-control" name="pic" />
                                            <span class="help-block">* </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab3">
                                    <h3 class="block">Payment Details</h3>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mobile money number
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control"
                                                   id="mobile_money_number" maxlength="10" required name="mobile_money_number" placeholder="Number in this format 0242953672" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mobile Money Network
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="mobile_network" id="mobile_network" required>
                                                <option value="mtn">Mtn</option>
                                                <option value="vodafone">Vodafone</option>
                                                <option value="tigo">Airtel/Tigo</option>
                                            </select>

{{--                                            <input type="text" class="form-control" name="mobile_network" id="mobile_network" />--}}
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                </div>

{{--                                <div class="tab-pane" id="tab4">--}}
{{--                                    <h3 class="block">Confirm details</h3>--}}

{{--                                    <div class="alert alert-danger print-error-msg" style="display:none">--}}

{{--                                        <ul></ul>--}}

{{--                                    </div>--}}

{{--                                    <h4 class="form-section">Attendee Details</h4>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Event:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" data-display="name"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Name of Attendee:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" data-display="sex"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Phone Number:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" id="phone_number"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Email:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" id="email"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Location:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" id="location"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Name of Handler:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" id="handler_name"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Category of interest:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" id="interest_category"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <h4 class="form-section">Dog(s) details</h4>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Name of dog:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" data-display="dog-name" id="dog_name" > </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Breed:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" data-display="breed"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Gender:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" data-display="gender"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Date of birth:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" id="dob" data-display="dob"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Image:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static"  data-display="image"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <h4 class="form-section">Payment Details</h4>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Mobile Money Number:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" data-display="mobile_money_number"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Mobile Network:</label>--}}
{{--                                        <div class="col-md-4">--}}
{{--                                            <p class="form-control-static" data-display="mobile_network"> </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                </div>--}}

                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back </a>
                                    <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                        <i class="fa fa-angle-right"></i>
                                    </a>

                                    <button onclick="payWithPaystack(event)" id="payment-button" type="button" class="btn green button-submit">
                                        Make Payment
                                        <i class="fa fa-check"></i>
                                    </button>

{{--                                    <button onclick="payWithPaystack(event)" id="make-payment-btn" type="button" class="btn green">Make payment--}}
{{--                                        <i class="fa fa-check"></i>--}}
{{--                                    </button>--}}
                                    {{--<a href="javascript:;" class="btn green button-submit"> Submit--}}
                                    {{--<i class="fa fa-check"></i>--}}
                                    {{--</a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
