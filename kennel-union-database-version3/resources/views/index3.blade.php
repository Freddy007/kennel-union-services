@extends("layouts.index3_layout")

@section('styles')
    <style>
        .job-listing-box .job-listing-footer .meta > li.location:before {
            display: block;
            position: absolute;
            left: -8px;
            top: 0;
            content: "\f0e3";
            font-family: 'FontAwesome';
            font-size: 14px;
            font-style: normal;
            font-weight: normal;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: #454545;
            width: 15px;
            text-align: right;
        }
    </style>
@endsection

@section("title")
    Home
    @endsection

@section("content")

    <!-- Main -->
    <div class="main" role="main">

        <!-- Slider -->
        <section class="slider-holder">
            <div class="container">
                <div class="flexslider carousel">
                    <ul class="slides">

                        @foreach($events as $event)
                            <li>
                                <img src="{{"images/slides/$event->banner_image"}}" alt="">
                            </li>
                        @endforeach
                        <li>
                            <img src="images/slide1.jpg" alt="">
                        </li>

                        <li>
                            <img src="images/slide3.jpg" alt="">
                        </li>
                    </ul>

                    <div class="search-box">
                        <h2>Search from the Pedigree Database </h2>
                        <form action="{{url("/search-results")}}" method="GET" role="form">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" required placeholder="name, registration or microchip number">
                            </div>

                            <div class="form-group">
                                <div class="select-style">
                                    <select class="form-control" name="sex">
                                        <option value="all">All sex</option>
                                        <option value="male">Sire</option>
                                        <option value="female">Dam</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="select-style">
                                    <select class="form-control" name="breed">
                                        <option value="all">All breeds</option>
                                        @foreach($breeds as $breed)
                                            <option value="{{$breed->id}}">{{$breed->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- Slider / End -->

        <!-- Page Content -->
        <section class="page-content">
            <div class="container">

                <!-- Light Section -->
                <section class="section-light section-nomargin">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="with-subtitle" data-animation="fadeInUp" data-animation-delay="0">Latest Uploads
{{--                                <small data-animation="fadeInUp" data-animation-delay="100">Subtitle goes here</small>--}}
                            </h2>
                            <div class="row">

                                @foreach($dogs as $dog)

                                    <div class="col-xs-6 col-sm-3 col-md-3" data-animation="fadeInLeft" data-animation-delay="0">
                                        <div class="job-listing-box featured">
                                            <figure class="job-listing-img">
                                                <a href="{{url('/show-dog',$dog->id)}}">
                                                    <img  src={{$dog->image_name == null ? url('https://via.placeholder.com/300x200/cccccc/000000?text=no+image+uploaded'):"/images/catalog/$dog->image_name"}}></a>
                                            </figure>
                                            <div class="job-listing-body">
                                                <h4 class="name"><a href="{{url('/show-dog',$dog->id)}}">{{ strtoupper($dog->name) }}</a></h4>
                                            </div>
                                            <footer class="job-listing-footer">
                                                <ul class="meta">
                                                    <li class="category">{{$dog->breeder_name}}</li>
                                                    @if($dog->assessed)
                                                      <li class="location"><i style="color: green">Assessed</i></li>
                                                    @else
                                                        <li class="location"><i style="color: red">Not assessed</i></li>
                                                    @endif
{{--                                                    <li class="date">Posted {{getDogRegisteredDay($dog->created_at)}}</li>--}}
                                                </ul>
                                            </footer>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </section>
                <!-- Light Section / End -->
            </div>
        </section>
        <!-- Page Content / End -->

        <!-- Footer -->
        <footer class="footer" id="footer">

            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            Copyright &copy; 2022 <a href="/">Kennel Union of Ghana</a> &nbsp;| &nbsp;All Rights Reserved
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <div class="social-links-wrapper">
                                <span class="social-links-txt">Keep in Touch</span>
                                <ul class="social-links social-links__light">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer / End -->

    </div>
    <!-- Main / End -->

@endsection
