
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Charm&display=swap" rel="stylesheet">
    <style>
        .hide-column{
            display: none;
        }

        .h5 {
            font-size: 2.2em !important;
            text-transform: uppercase;
        }

        .h6{
            font-size: 1.60em !important;
            text-transform: uppercase;
        }

        .water-mark{
            /*z-index: 20000;*/
            position: absolute;
            top: 250px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0.1;
        }

        .table-container{
            border: 15px solid  rgb(172, 209, 165);
        }


        .male {
            background-color: 	rgba(200, 247, 197, 0.5);
            color: #000000;
            text-align: left;
            vertical-align: middle;
            border-collapse: collapse;
            /*border: 1px solid #000000;*/
            border-bottom: 5px solid 	rgba(200, 247, 197, 0.6);
            border-top: 5px solid rgba(200, 247, 197, 0.6);
            border-right: 5px solid rgba(200, 247, 197, 0.6);
            padding-left: 10px;

        }

        .female {
            background-color: rgba(255,255,255,0.5);
            color: #000000;
            /*font-family: Sans Serif;*/
            /*font-size: 8pt;*/
            text-align: left;
            vertical-align: middle;
            border-collapse: collapse;
            /*border: 1px solid #000000;*/
            /*border: 5px solid rgba(228, 155, 15,0.6);*/
            border-bottom: 5px solid rgba(200, 247, 197, 0.6);
            border-top: 5px solid rgba(200, 247, 197, 0.6);
            border-right: 5px solid rgba(200, 247, 197, 0.6);
            padding-left: 10px;
        }

        table,.dog-info{
            margin:auto;
        }

        hr{
            width: 94%;
            margin:auto;
        }

        @font-face {
            font-family: 'Charm';
            src: url({{ storage_path('fonts\Charm-Regular.ttf') }});
        }


    </style>

</head>


<div class="content">

    <div class="container-fluid">

        <div class="portlet portlet-default">

            <div class="portlet-body main-table">

                <div class="table-container"
                     style="margin-top: -45px; margin-left: -60px; margin-right: -60px;
                      padding-top: 10px;" >
                    {{--                      padding-top: 10px; padding-bottom: 110px;">--}}

                    <h1 style="text-align: center; font-size: 4em; font-family: 'Comic Sans MS','cursive'"> Pedigree Certificate</h1>
                    <br>

                    <div class="row">

                        <div class="row water-mark"></div>

                        <div class="water-mark" style="">
{{--                            <img src="{{public_path('img') ."/lion_kennel.jpeg"}}" width="2200px" height="2200px"/>--}}
                            <img src="{{public_path('img') ."/kug_logo_2.png"}}" width="2200px" height="2200px"/>
                        </div>

                        <div class="col-md-12">
                            <table width="100%" >
                                <tr style=" background-color: rgba(200, 247, 197, 0.6);">
                                    <th style="text-align: left; padding-left: 10px; height: 50px; font-size: 2em; ">
                                        {{--                                        NAME: {{(strtoupper(strtolower($dog->name)))}}--}}
                                        {{--                                        NAME: {{(strtoupper(strtolower(mb_substr($dog->name, 0, 20, 'utf8').'...')))}}--}}
                                        NAME: {{strtoupper(strtolower($dog->name))}}
                                    </th>
                                    <th style="text-align: center; height: 50px; font-size: 2em;">
                                        BREED: {{(strtoupper(strtolower($dog->breed)))}}
                                    </th>
                                    <th style="text-align: center; height: 50px; font-size: 2em;">
                                        BREEDER: {{strtoupper(strtolower($dog->first_name))}}  {{strtoupper($dog->last_name)}}</th>
                                </tr>
                                <tr>
                                    <th style="text-align: left; padding-left: 10px; height: 50px; font-size: 2em;">
                                        DATE OF BIRTH: {{strtoupper($dog->dob)}}</th>
                                    <th style="text-align: center; height: 50px"></th>
                                    <th style="text-align: center; height: 50px; font-size: 2em;">
                                        OWNER: </th>
                                </tr>

                                <tr style=" background-color: rgba(200, 247, 197, 0.6);">
                                    <th style="text-align: left; padding-left: 10px; height: 50px; font-size: 2em;">
                                        SEX: {{strtoupper($dog->sex)}}</th>

                                    <th style="text-align: center; height: 50px"></th>
                                    <th style="text-align: center; height: 50px; font-size: 2em;">
                                        MICROCHIP: {{$dog->microchip_number}}</th>
                                </tr>

                                <tr>
                                    <th style="text-align: left; padding-left: 10px; height: 50px; font-size: 2em;">
                                        COAT: {{strtoupper($dog->coat)}}
                                    </th>

                                    <th style="text-align: center; height: 50px; font-size: 2em;">REGISTRATION NO: {{strtoupper($dog->registration_number)}}</th>
                                    <th style="text-align: center; height: 50px; font-size: 2em;">
                                        DATE OF ISSUE:<em> <?php echo date('d-m-y'); ?> </em></th>
                                </tr>

                                <tr style="background-color: rgba(200, 247, 197, 0.6);">
                                    <th style="text-align: left; padding-left: 10px; height: 50px; font-size: 2em;">
                                        COLOUR: {{strtoupper($dog->colour)}}</th>

                                    <th style="text-align: center; height: 50px"></th>
                                    <th style="text-align: center; height: 50px; font-size: 2em;">
                                    </th>
                                </tr>

                            </table>
                        </div>

                    </div>

                    <div>

                        <table border="0" cellpadding="2" width='100%' style="margin-top: 30px; border: 0;" >
                            <tr>
                                <td>

                                    <table
                                            style=" border-collapse: collapse; border-bottom: 5px solid rgba(228, 155, 15,0.6);"  cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                                        <tr>
                                            <th style="text-align: center; font-size: 2em">PARENTS</th>
                                            <th style="text-align: center; font-size: 2em">GRAND PARENTS</th>
                                            <th style="text-align: center; font-size: 2em">GREAT GRAND PARENTS</th>
                                            <th style="text-align: center; font-size: 2em">GREAT GRAND GRAND PARENTS</th>
                                        </tr>


                                        <tr>
                                            <td rowspan='16' width='17%' class='male'>
                                                <div class="h5">
                                                    @if ($dog->father)
                                                        {{\App\Dog::getRelationship($dog->father)}}<br>
                                                        {{$dog->father}}<br>
                                                        @php
                                                            $exist = \App\Dog::whereRegistrationNumber($dog->father)->first();
                                                        @endphp
                                                        @if($exist)
                                                            {{$exist->other_registration_number}}
                                                            | {{strtoupper($exist->titles)}}

                                                        @endif


                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($dog->father,'father')}}<br/>
                                                    {{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($secondgen)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}

                                                    @endif


                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($secondgen,'father')}}<br/>
                                                    {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($third_generation)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}

                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h6">
                                                    {{\App\Dog::getParent($third_generation,'father')}}<br/>
                                                    {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($fifth_generation)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}

                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h6">
                                                    {{\App\Dog::getParent($third_generation,'mother')}}<br/>
                                                    {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($secondgen,'mother')}}<br/>
                                                    {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($fourth_generation)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h6">

                                                    {{\App\Dog::getParent($fourth_generation,'father')}}<br/>
                                                    {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_b)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h6">
                                                    {{\App\Dog::getParent($fourth_generation,'mother')}}<br>
                                                    {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_c)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($dog->father,'mother')}} <br>
                                                    {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($thirdgen_mother)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($thirdgen_mother,'father')}}<br>
                                                    {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($fourth_gen)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h6">{{\App\Dog::getParent($fourth_gen,'father')}}<br/>
                                                    {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_d)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h6">{{\App\Dog::getParent($fourth_gen,'mother')}}<br/>
                                                    {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_e)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>


                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($thirdgen_mother,'mother')}}<br/>
                                                    {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_four)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h6">{{\App\Dog::getParent($generation_four,'father')}}<br/>
                                                    {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_f)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h6">
                                                    {{\App\Dog::getParent($generation_four,'mother')}}<br/>
                                                    {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_g)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='16' width='17%' class='female' >

                                                <div class="h5">

                                                    @if ($dog->mother)
                                                        {{\App\Dog::getRelationship($dog->mother)}}<br/>
                                                        {{$dog->mother}}<br>

                                                        @php
                                                            $exist = \App\Dog::whereRegistrationNumber($dog->mother)->first();
                                                        @endphp
                                                        @if($exist)
                                                            {{$exist->other_registration_number}}
                                                            <br> {{strtoupper($exist->titles)}}
                                                        @endif
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($dog->mother,'father')}}<br/>
                                                    {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_two_a)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($generation_two_a,'father')}}<br/>
                                                    {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_two_c)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h6">{{\App\Dog::getParent($generation_two_c,'father')}}<br/>
                                                    {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_4_d)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h6">{{\App\Dog::getParent($generation_two_c,'mother')}}<br/>
                                                    {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_a)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_two_a,'mother')}}<br/>
                                                    {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_3_a)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h6">{{\App\Dog::getParent($generation_3_a,'father')}}<br/>
                                                    {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_e)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h6">{{\App\Dog::getParent($generation_3_a,'mother')}}<br/>
                                                    {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_f)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($dog->mother,'mother')}}<br/>
                                                    {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_two_b)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($generation_two_b,'father')}}<br/>
                                                    {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_3_c)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br> {{strtoupper($exist->titles)}}

                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h6">{{\App\Dog::getParent($generation_3_c,'father')}}<br/>
                                                    {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_h)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h6">{{\App\Dog::getParent($generation_3_c,'mother')}}<br/>
                                                    {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_i)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}

                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='4' width='17%' class='female' style="border-left: none;">
                                                <div class="h5" style="margin-top: -15px; ">
                                                    {{\App\Dog::getParent($generation_two_b,'mother')}}<br/>
                                                    {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_4_b)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        <br>{{strtoupper($exist->titles)}}
                                                    @endif
                                                </div>
                                            </td>

                                            <td rowspan='1' width='17%' class='male'>
                                                <div class="h6">{{\App\Dog::getParent($generation_4_b,'father')}}<br/>
                                                    {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_j)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                        31

                                                    @endif
                                                </div>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td rowspan='1' width='17%' class='female'>
                                                <div class="h6">{{\App\Dog::getParent($generation_4_b,'mother')}}<br/>
                                                    {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'mother','registration_number')}} |

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_j)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                        | {{strtoupper($exist->titles)}}
                                                        32
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            {{--                                <table width="100%" >--}}
                            {{--                                    <tr class="female">--}}
                            {{--                                        <th style="text-align: center; height: 100px; font-size: 20px;">--}}
                            {{--                                            Breed: {{ucwords(strtolower($dog->breed))}}--}}
                            {{--                                        </th>--}}
                            {{--                                    </tr>--}}
                            {{--                                </table>--}}

                        </table>

                        <table width="100%" style="margin-top:-20px; " >
                            <tr style=" ">
                                <th style="text-align: center; height: 120px">
                                    <img src="{{public_path('img') ."/kug_logo_2.png"}}" width="150px" height="138px"/>

                                </th>

                                <th style="text-align:center; font-size: 2.5em; font-weight: lighter; padding-left: 50px; ">
                                    Kennel Union of Ghana. https://www.kennelunionofghana.com</th>
                                </th>

                                <th style="padding-right: 120px;">
                                    <span style="font-size: 2.5em;">Signed: <img width="80px" height="45px"
                                                                                 src="{{public_path('img') ."/kug_signature2.png"}}" alt=""/></span></th>
                            </tr>


                        </table>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</html>

