@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/searchpanes/1.2.0/css/searchPanes.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css" rel="stylesheet" type="text/css" />

    <style>
        tfoot input {
            width: 100%;
            padding: 3px;
            box-sizing: border-box;
        }
    </style>
@endsection

@section('scripts')
    {{--<script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>--}}

    <script src="{{asset('kug_version2/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>

<script src="//code.highcharts.com/highcharts.js" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            // Search Functionality
            $('#searchForm').submit(function(event) {
                event.preventDefault(); // Prevent form submission
                var searchText = $(this).find('input[name="search"]').val();
                var url = "{{ url('/version2/confirmed-members') }}?search=" + searchText;
                window.location.href = url; // Redirect to the search URL
            });
        });
    </script>


@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Confirmed Members
                    </h1>
                </div>
                <div class="page-toolbar">

                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Confirmed Members</span>
                    </li>
                </ul>

                <div class="page-content-inner">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">

                                        <form id="searchForm">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search..." value="{{ Request::query("search") }}" name="search">
                                                <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
                <i class="fa fa-search"></i>
            </button>
        </span>
                                            </div>
                                            @if (Request::has('search'))
                                                <input type="hidden" name="search" value="{{ Request::query('search') }}">
                                            @endif
                                        </form>

                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                        <thead>
                                        <tr>
                                            <th>Member Id.</th>
                                            <th> Name</th>
                                            <th>Kennel Name</th>
                                            <th>Phone</th>
                                            <th>Registered Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        @foreach($members as $member )
                                            <tr>

                                                <td>
                                                    {{$member->member_id}}

                                                </td>
                                                <td>
                                                    {{$member->first_name}} {{$member->last_name}}
                                                </td>

                                                <td>
                                                    {{$member->kennel_name}}
                                                </td>

                                                <td>
                                                    {{$member->phone}}
                                                </td>

                                                <td>
                                                    {{$member->created_at}}
                                                </td>

                                                <td></td>

                                            </tr>
                                        @endforeach

                                    </table>

                                    <div class="text-center">
                                        <ul class="pagination-custom list-unstyled list-inline">

                                            <div class="text-center">
                                                <ul class="pagination-custom list-unstyled list-inline">
                                                    {!! $members->appends(['search' => Request::get('search')])->render() !!}
                                                </ul>
                                            </div>

                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
