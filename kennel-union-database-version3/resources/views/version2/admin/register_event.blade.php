@extends('version2.layouts.admin_layout')

@section('styles')

@endsection

@section('scripts')

    <script src="{{asset('kug_version2/assets/global/scripts/select2.min.js')}}" type="text/javascript"></script>

    <script>
        $("#user_id").select2();

    </script>

@endsection


@section('content')
        <!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Register Event
                        {{--<small>managed datatable samples</small>--}}
                    </h1>
                </div>
                <!-- END PAGE TITLE -->

            </div>
        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{url('/version2/all-dogs')}}">all events</a>
                        <i class="fa fa-circle"></i>
                    </li>

                    <li>
                        <span>register event</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">

                    <div class="row">
                        <div class="col-md-12">
                            @include('flash::message')
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        {{--<i class="icon-equalizer font-red-sunglo"></i>--}}
                                        <span class="caption-subject font-red-sunglo bold uppercase"></span>
                                        {{--<span class="caption-helper">{{$event->name}}</span>--}}
                                    </div>
                                    <div class="actions">
                                        <div class="portlet-input input-inline input-small">
                                            <div class="input-icon right">
                                                {{--<i class="icon-magnifier"></i>--}}
                                                {{--<input type="text" class="form-control input-circle" placeholder="search..."> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="/version2/events" class="form-horizontal" method="post" enctype="multipart/form-data">

                                        {!! csrf_field() !!}

                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="name" placeholder="Name of event" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description</label>
                                                <div class="col-md-4">
                                                    <textarea class="form-control" name="description" required></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Date of event</label>
                                                <div class="col-md-4">
                                                    <input type="date" class="form-control" placeholder="Date of event" name="event_date" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Deadline for registration</label>
                                                <div class="col-md-4">
                                                    <input type="date" class="form-control" placeholder="Date of event" name="deadline_date" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Event fee (Type 0 if free)</label>
                                                <div class="col-md-4">
                                                    <input type="number"  class="form-control" name="event_fee" placeholder="Event fee" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Assessment Fee per dog for members (Type 0 if free)</label>
                                                <div class="col-md-4">
                                                    <input type="number"  class="form-control" name="member_assessment_fee" placeholder="Member's dog assessment fee " required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Assessment Fee per dog for non-members (Type 0 if free)</label>
                                                <div class="col-md-4">
                                                    <input type="number"  class="form-control" name="non_member_assessment_fee" placeholder="Non member's dog assessment fee">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Participants</label>
                                                <div class="col-md-4">
                                                  <select class="form-control" name="participants">
                                                      <option value="everyone">Everyone</option>
                                                      <option value="members-only">Only Members</option>
                                                  </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Event's Banner <small>size: 1120 * 480 </small></label>
                                                <div class="col-md-4">
                                                    <input type="file" class="form-control" name="banner_image" required placeholder="Event's banner" >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Event's Location</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="location name" placeholder="Location of event" >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Event's Coordinates(eg. lat, long)</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="location_coordinates" placeholder="latitude, longitude" >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Contact Number for event</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="contact_number" placeholder="Contact number" >
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Save event</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

@endsection
