@extends('version2.layouts.admin_layout')

@section('styles')
    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{"/css/all_dogs.css"}}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/0.11.1/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

@endsection


@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>All Events
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->


            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content" style="min-height: 100%">

                <div id="overlay"><h2>Loading .. Please wait</h2></div>

                <div class="container-fluid">
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>

                            <span>All Events</span>
                        </li>
                    </ul>

                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--                                            <i class="icon-settings font-dark"></i>--}}
{{--                                            <span class="caption-subject bold uppercase"> {{\App\Event::count()}} dogs registered</span>--}}
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{url('version2/register-event')}}" id="sample_editable_1_new" class="btn sbold green"> Add New Event
{{--                                                            <i class="fa fa-plus"></i>--}}
                                                        </a>


{{--                                                        <a href="{{url('version2/register-litter')}}" id="sample_editable_1_new" class="btn sbold yellow"> Add Litter--}}
{{--                                                            <i class="fa fa-plus"></i>--}}
{{--                                                        </a>--}}


                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row margin-bottom-10">

                                        </div>


                                        <table class="table table-striped table-bordered table-hover order-column">
                                            <thead>
                                            <tr>
                                                {{--                                                <th>Image</th>--}}
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Ongoing</th>
                                                <th>Date of Event</th>
                                                <th>Deadline for Registration</th>
                                                <th>Event fee</th>
                                                <th>Member Assessment fee</th>
                                                <th>Non Member Assessment fee</th>
                                                <th>Participants</th>
                                                <th>Location name</th>
                                                <th>Contact number</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            @foreach($events as $event )
                                                <tr style="{{$event->archived ? 'background-color: #D89825; color:#fff' : 'background-color: #fff' }}">
                                                    <td>{{$event->created_at}}</td>
                                                    <td>{{ strtolower($event->name) }}</td>
                                                    <td>{{"Yes"}}</td>
                                                    <td>{{ $event->event_date  }}</td>
                                                    <td>{{ $event->deadline_date }}</td>
                                                    <td style="text-align: center">GHS {{ number_format($event->event_fee)}}</td>
                                                    <td style="text-align: center">GHS {{ number_format($event->member_assessment_fee)}}</td>
                                                    <td style="text-align: center">GHS {{ number_format($event->non_member_assessment_fee)}}</td>
                                                    <td>{{ $event->participants}}</td>
                                                    <td>{{ $event->location_name}}</td>
                                                    <td>{{ $event->contact_number}}</td>
                                                    <td>
                                                        <div>
                                                            <a href="{{url("/version2/attendees",$event->id)}}" class="btn btn-success">View attendees</a>
                                                        </div>
                                                        <br/>
                                                        <div>
                                                            <button class="btn btn-default">edit</button>
                                                            <br/>
                                                            <br/>
                                                            @if(!$event->archived)
                                                            <a href="/version2/archive-event/{{$event->id}}" class="btn btn-danger">archive</a> <br/>
                                                            @else
                                                            <a href="/version2/unarchive-event/{{$event->id}}" class="btn btn-success">unarchive</a> <br/>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>

                                        <div class="text-center">
                                            <ul class="pagination-custom list-unstyled list-inline">

                                                {!! $events->render() !!}


                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection
