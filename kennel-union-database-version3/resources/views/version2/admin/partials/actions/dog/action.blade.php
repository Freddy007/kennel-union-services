<span><a href="dog/{{$dog->id}}" class='btn btn-default btn-xs'><i class='fa fa-eye'></i></a></span>

<span><a href="edit-dog/{{$dog->id}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o"></i> </a></span>

<span><button class="btn btn-danger btn-xs delete-request"  data-id="{{$dog->id}}"  data-toggle="tooltip" data-placement="left" title="move {{$dog->name}} to trash"><i class="fa fa-trash"></i></button></span>
