
let substringMatcher = function(strings) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        matches = [];
        substrRegex = new RegExp(q, 'i');

        $.each(strings, function(i, string) {
            if (substrRegex.test(string)) {
                matches.push(string);
            }
        });

        cb(matches);
    };
};

// Suggestion engine
const courses = new Bloodhound({
    datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
    queryTokenizer: Bloodhound.tokenizers.whitespace,

    prefetch: "{{url('/type-ahead-dogs?q=%QUERY')}}",
    remote: {
        url: "{{url('/type-ahead-dogs?q=%QUERY')}}",
        wildcard: '%QUERY',
        filter: courses => $.map(courses, dog => ({
            value: dog.name,
            id: dog.id,
            registration_number: dog.registration_number,
            sex: dog.sex
        }))
    }
});

courses.initialize();
// Instantiate the Typeahead UI
$('#name').typeahead(null, {
    displayKey: 'value',
    display: 'registration_number',
    source: courses.ttAdapter(),
    templates: {
        empty: [
            '<div class="empty-message">',
            'unable to find any dog that matches the current query',
            '</div>'
        ].join('\n'),
        suggestion: Handlebars.compile('<div><strong>@{{value}}</strong> – @{{registration_number}} – @{{sex}}</div>')
    }
});

function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
        '<td>Registration date:</td>'+
        '<td>'+d.created_at+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Microchip number:</td>'+
        '<td>'+d.microchip_number+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Tattoo number:</td>'+
        '<td>'+d.tattoo_number+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Father:</td>'+
        '<td>'+d.father+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Mother:</td>'+
        '<td>'+d.mother+'</td>'+
        '</tr>'+
        '</table>';
}

jQuery(document).ready(function() {

    var query = "{{$query}}";

    var table = $('#datatable-buttons').DataTable({
        "aaSorting": [],
        "iDisplayLength": 10,
        pageLength: 10,
        processing: true,
        serverSide: true,
        ordering: true,
        ajax: 'all-dogs-data' + query,
        columns: [
            {
                "className": 'details-control',
                "data": null,
                "defaultContent": ''
            },
            {data: "name", name: 'name'},
            {data: "siblings", name: 'siblings', orderable: true},
            {data: "offspring", name: 'offspring'},
            {data: "dob", name: 'dob'},
            {data: "breed", name: 'breed'},
            {data: "sex", name: 'sex', searchable: false},
            {data: "registration_number", name: 'registration_number'},
            {data: 'breeder', name: 'breeder'},
            {data: "status", name: 'status', searchable: false, printable: false},
            {data: "delete_request", name: 'delete_request', searchable: false, printable: false},
            {data: "certificate", name: 'certificate', searchable: false, orderable: false},
            {
                data: 'action',
                name: 'action',
                searchable: false,
                orderable: false,
                printable: false,
                exportable: false
            }
        ],
        "oLanguage": {
            "sInfo": "_START_ to _END_ of _TOTAL_ entries",
            "sLengthMenu": "Show _MENU_",
            "sSearch": "",
            "sEmptyTable": "No data found!",
        },
        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": [-1]
        }],
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],     // page length options
        "initComplete": function (settings, json) {
//                    addButtons();

        },
        "createdRow": function (row, data, dataIndex) {
            if (data[1] !== `0`) {
                $(row).addClass('redClass');
            }

            if(row === 2){
                let id = data.id
                location.href = `/version2/sibling-dogs/${id}`;
            }
            console.log(dataIndex);
            console.log(row);
            console.log(data);
            console.log( data.id);
        }
    });

    $('#datatable-buttons tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    $('#datatable-buttons tbody').on( 'click', 'tr', function () {
        console.log( table.row( this ).data() );
        let id = table.row(this).data().id;
        // location.href = `/version2/sibling-dogs/${id}`;
    } );

    table.on('draw.dt', function () {
        console.log('Redraw occurred at: ' + new Date().getTime());
        $('.owner').hide();

        $('.add-owner-detail').on('click', function (e) {
            // e.preventDefault();
            var dog_id = $(this).data('id');
            var dog_name = $(this).data('name');

            $('#new_owner').on('change', function () {
                if ($(this).is(":checked")) {
                    $('.owner').show()
                } else {
                    $('.owner').hide()
                }
            });
        });

        $(".confirm").on("click", function () {
            var confirmed = $(this).data("confirmed");
            var name = $(this).data("title");
            var id = $(this).data("id");
            let page = table.page.info().page;

            console.log( page);

            Swal.fire({
                title: 'Are you sure?',
                text: "This dog will be confirmed!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, confirm this dog!'
            }).then((result) => {
                if (result.isConfirmed) {
                    confirmDog(id);
                    table.page(page).draw('page');
                    // table.draw();
                }
            })
        });

        $(".delete-request").off().on("click", function () {
            let dog_id = $(this).data("id");
            let page = table.page.info().page;

            Swal.fire({
                title: 'Are you sure?',
                text: "This dog will be moved to archives!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    archiveDog(dog_id);
                    table.page(page).draw('page');
                }

            })
        })

        $(".approve-certificate").off().on("click", function () {
            let dog_id = $(this).data("id");
            let page = table.page.info().page;

            Swal.fire({
                title: 'Are you sure?',
                text: "This will approve certificate request!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Approve it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    approveCertificate(dog_id);
                    table.page(page).draw('page');
                }

            });
        });

        $(".download-certificate").off().on("click", function (event) {
            event.preventDefault();
            const download_base_url = "http://18.194.254.77/certification/public";

            let dog_id = $(this).data("id");
            enterNewOwner(dog_id);

            $(document).on('click', '.download', function() {
                let name = $("#swal2-input").val();
                Swal.fire('Downloading...', '','info');
                if (name) {
                    window.location.href = `${download_base_url}/${dog_id}?name=${name}`;
                } else {
                    window.location.href = `${download_base_url}/${dog_id}`;
                }
                console.log('Button 1');
            });
            $(document).on('click', '.sendToMember', function() {
                console.log('Button 2');
                let name = $("#swal2-input").val();
                approveCertificate(dog_id, name);
                Swal.fire('Certificate will be sent to the member', '','info');
            });
        })
    });

    function addButtons() {
        table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    }

    function confirmDog(id) {
        $.post("/version2/confirm-dog/"+id, function (data) {
            Swal.fire(
                'Confirmed!',
                'Dog is now confirmed!',
                'success'
            )

        });
    }

    function archiveDog(id) {
        $.post("/version2/archive-dog/" + id, function (data) {
            Swal.fire('Archived!', 'Dog is now archived.', 'success')
        });
    }

    function approveCertificate(id,owner_name) {
        let url = name === ""? "/version2/approve-certificate-request/" + id : "/version2/approve-certificate-request/" + id + "?name="+owner_name;
        $.post(url, function (data) {
            Swal.fire(
                'Approved!',
                'Download url has been generated and send to member.',
                'success'
            )
        });
    }

    async function enterNewOwner(dog_id){
        const download_base_url = "http://18.194.254.77/certification/public";
        const { value: name } = await Swal.fire({
            title: "Enter new owner's name",
            html:
                '<input maxlength="100" autocapitalize="off" autocorrect="off" class="swal2-input" id="swal2-input" placeholder="Enter name(not required)" type="text" style="display: flex;">' +
                '<button type="button" role="button" tabindex="0" class="SwalBtn1 download customSwalBtn">' + 'Download' + '</button>' +
                '<button type="button" role="button" tabindex="0" class="SwalBtn2 sendToMember customSwalBtn">' + 'Send to member' + '</button>',
            showCancelButton: false,
            showConfirmButton: false
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            let owner_name = result.value;
            console.log(result);

            if (result.isDenied) {
                approveCertificate(dog_id, owner_name);
                Swal.fire('Certificate will be sent to the member '  + owner_name, '', 'info');
            }

            if (result.isConfirmed) {
                Swal.fire("Downloading..." + owner_name, '', 'success');
                if (owner_name) {
                    window.location.href = `${download_base_url}/${dog_id}?name=${owner_name}`;
                }else{
                    window.location.href = `${download_base_url}/${dog_id}`;
                }
            }
        });
    }


    // async function enterNewOwner(dog_id){
    //     const download_base_url = "http://18.194.254.77/certification/public";
    //     const { value: name } = await Swal.fire({
    //         title: "Enter new owner's name",
    //         html: "Some Text" +
    //             "<br>" +
    //             '<button type="button" role="button" tabindex="0" class="SwalBtn1 customSwalBtn">' + 'Button1' + '</button>' +
    //             '<button type="button" role="button" tabindex="0" class="SwalBtn2 customSwalBtn">' + 'Button2' + '</button>',
    //         showDenyButton: true,
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         denyButtonColor: '#29ADB9',
    //         confirmButtonText: `Download`,
    //         denyButtonText: `Send to member`,
    //         input: 'text',
    //         inputLabel: "Owner's name",
    //         inputPlaceholder: 'Enter name(not required)',
    //         inputAttributes: {
    //             maxlength: 100,
    //             autocapitalize: 'off',
    //             autocorrect: 'off'
    //         }
    //     }).then((result) => {
    //         /* Read more about isConfirmed, isDenied below */
    //         let owner_name = result.value;
    //         console.log(result);
    //
    //         if (result.isDenied) {
    //             approveCertificate(dog_id, owner_name);
    //             Swal.fire('Certificate will be sent to the member '  + owner_name, '', 'info');
    //         }
    //
    //         if (result.isConfirmed) {
    //
    //             Swal.fire("Downloading..." + owner_name, '', 'success');
    //
    //             if (owner_name) {
    //                 window.location.href = `${download_base_url}/${dog_id}?name=${owner_name}`;
    //             }else{
    //                 window.location.href = `${download_base_url}/${dog_id}`;
    //             }
    //         }
    //     });
    //
    //     // swal({
    //     //     title: 'Title',
    //     //     html: "Some Text" +
    //     //         "<br>" +
    //     //         '<button type="button" role="button" tabindex="0" class="SwalBtn1 customSwalBtn">' + 'Button1' + '</button>' +
    //     //         '<button type="button" role="button" tabindex="0" class="SwalBtn2 customSwalBtn">' + 'Button2' + '</button>',
    //     //     showCancelButton: false,
    //     //     showConfirmButton: false
    //     // });
    //
    // }


});

