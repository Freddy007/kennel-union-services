<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->uuid("id");
            $table->string("name");
            $table->string("description");
            $table->string("banner_image")->nullable();
            $table->string("location_name");
            $table->dateTime("event_date");
            $table->dateTime("deadline_date");
            $table->decimal("event_fee");
            $table->decimal("member_assessment_fee");
            $table->decimal("non_member_assessment_fee");
            $table->enum("participants",["everyone", "only_members"]);
            $table->string("contact_number");
            $table->string("location_coordinates")->nullable();
            $table->boolean("archived")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
