<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendees', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->uuid("event_id")->index();
            $table->string("full_name");
            $table->string("phone_number");
            $table->string("email");
            $table->string("location");
            $table->string("handler_name")->nullable();
            $table->enum("category_of_interest",["main-show","assessment"])->nullable();
            $table->string("mobile_money_number")->nullable();
            $table->enum("mobile_network",["mtn","tigo","vodafone"])->nullable();
            $table->string("kug_id")->nullable();
            $table->string("transaction_id")->nullable();
            $table->decimal("amount")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendees');
    }
}
