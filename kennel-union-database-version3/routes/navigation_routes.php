<?php
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 7/13/2017
 * Time: 11:49 AM
 */

Menu::make('AdminNav',function($menu){
//    $navigation_menu = array('Dashboard','All Members','Dogs','Breeds','Certificates',"Archives","Duplicates");
//    $navigation_menu = array('Dashboard','All Members','Dogs','Downloads','Breeds',"Archives","Duplicates");
    $navigation_menu = array('Dashboard','All Members','Dogs','Breeds',"Duplicates","Events");

    $navigation_links = array('Dashboard' => 'version2',
                              'All Members'=>'version2/all-members',
                              'Dogs' => 'version2/all-dogs',
//                              'Trash' => 'admin/trash',
                                // 'Downloads' => 'version2/downloads',
                                  'Breeds' => 'version2/all-breeds',
                                   'Events' => 'version2/events',

//                              'Certificates' => 'version2/certificate-requests',
//                              'Archives' => 'version2/archives',
                              'Duplicates' => 'version2/duplicates'
    );

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu], 'class' => 'menu-dropdown classic-menu-dropdown'));
        $about->link->attr(array('class' => 'nav-link'));
    }
});


Menu::make('SuperAdminNav',function($menu){
        $navigation_menu = array('Dashboard','All Members','Confirmed Members','Dogs','Downloads','Exports','Breeds',"Duplicates", "Events");
    
        $navigation_links = array('Dashboard' => 'version2',
                                  'All Members'=>'version2/all-members',
                                  'Confirmed Members'=>'version2/confirmed-members',

            'Dogs' => 'version2/all-dogs',
    //                              'Trash' => 'admin/trash',
                                    'Downloads' => 'version2/downloads',
                                    'Exports' => 'version2/export-downloads',
                                      'Breeds' => 'version2/all-breeds',
                                       'Events' => 'version2/events',
    //                              'Archives' => 'version2/archives',
                                  'Duplicates' => 'version2/duplicates'
        );
    
        foreach($navigation_menu as $nav_menu){
            $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu], 'class' => 'menu-dropdown classic-menu-dropdown'));
            $about->link->attr(array('class' => 'nav-link'));
        }
    });


Menu::make('GuestMainPageNav',function($menu){
    $navigation_menu = array('Home','Contact Us','Login', "Register", "Event Registration");
    $navigation_links = array('Home' => '',
                              'Contact Us'=>'contact-us',
                              'Event Registration'=> 'event-registration',
                              'Login' => 'login',
                              'Register' => 'register',
    );

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
        $about->link->attr(array('class' => 'nav-link'));
    }
});

Menu::make('MainPageNav',function($menu){
    $navigation_menu = array('Home','Dashboard','Contact Us',"Event Registration");
    $navigation_links = array('Home' => '',
                              'Dashboard' => 'version2/all-dogs',
                              "Event Registration" => 'event-registration',
                              'Contact Us'=>'contact-us'
    );

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
        $about->link->attr(array('class' => 'nav-link'));
    }
});

Menu::make('MemberNav',function($menu){
    $navigation_menu = array('Home','Member page',"Contact Us");
    $navigation_links = array('Home' => '','Member page' => 'member', 'Contact Us'=>'contact-us');

    foreach($navigation_menu as $nav_menu){
        $about = $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu], 'class' => 'menu-dropdown classic-menu-dropdown'));
        $about->link->attr(array('class' => 'nav-link'));
    }
});

Menu::make('HomeNav1',function($menu){
    $navigation_menu = array('HOME','LOG IN');
    $navigation_links = array('HOME' => '','LOG IN' => 'auth/login');

    foreach($navigation_menu as $nav_menu){
        $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
    }
});

Menu::make('HomeNav2',function($menu){
    $navigation_menu = array('HOME','DASHBOARD','LOGOUT');
    $navigation_links = array('HOME' => '','DASHBOARD' => 'version2','LOGOUT'=>'logout');

    foreach($navigation_menu as $nav_menu){
        $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
    }
});

Menu::make('HomeNav3',function($menu){
    $navigation_menu = array('HOME','MEMBER PAGE','LOGOUT');
    $navigation_links = array('HOME' => '','MEMBER PAGE' => 'member','LOGOUT' => 'logout');

    foreach($navigation_menu as $nav_menu){
        $menu->add("$nav_menu",    array('url'  => $navigation_links[$nav_menu]));
    }
});
