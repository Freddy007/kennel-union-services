<?php

namespace App\Jobs;

use App\Dog;
use App\DogGeneration;
use App\DogRelationship;
use App\HelperClasses\DogHelper;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateOffspringCountJob
{
    use Dispatchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Dog::select('id', 'name', 'registration_number')->chunk(100, function($dogs){
            foreach ($dogs as $dog) {
                $offspringCount = DogRelationship::orWhere("father", $dog->registration_number)
                    ->orWhere("mother", $dog->registration_number)->count();
                Dog::find($dog->id)->update([
                    "offspring_count" => $offspringCount
                ]);
            }
        });
    }
}
