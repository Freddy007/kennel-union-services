<?php
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 1/17/2018
 * Time: 1:52 PM
 */

namespace App\HelperClasses;


use App\Events\Event;
use Webpatser\Uuid\Uuid;

trait Uuids {

	/**
	 *
	 * Boot function from laravel
	 */

	protected static function boot() {
		parent::boot();

		static::creating( function ( $model ) {
			$model->{$model->getKeyName()} = Uuid::generate()->string;
		});
	}

}