<?php

namespace App\Http\Controllers;

use App\Download;
use App\Jobs\GenerateCertificate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use function React\Promise\all;

class DownloadsController extends Controller
{
    //

//    public function index(){
//        $downloads = Download::orderByDesc("created_at")->paginate(15);
//        return view("version2.admin.downloads",compact("downloads"));
//    }

    public function index(Request $request){
        // Get the search query from the request
        $search = $request->input('search');

        // Query downloads with search functionality
        $query = Download::orderByDesc("created_at");

        // Apply search filter if search query exists
        if (!empty($search)) {
            $query->where(function($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');
                // Add more search conditions if needed
            });
        }

        // Paginate the results
        $downloads = $query->paginate(15);

        // Pass the downloads to the view
        return view("version2.admin.downloads", compact("downloads"));
    }


    public function saveCertificate(Request $request, $id){
        $fields = $request->all();
        $name = $fields["name"] ?? "";
        $address = $fields["address"] ?? "";
        $exportPedigree = $fields["includePedigree"] ?? "";
        GenerateCertificate::dispatch($id, $fields, $name, $address, $exportPedigree);
        return redirect()->back()->with("success","Certificate generation queued. Check Downloads to download your certificate! ");
    }


    public function downloadCertificate(Request $request, $id){
        $download = Download::whereDogId($id)->latest()->first();
        $file_path = public_path("certificates")."/".$download->file_path;
        return response()->download($file_path,$download->file_path);
    }

    public function exports(Request $request){
        // Get the search query from the request
        $search = $request->input('search');

        // Query downloads with search functionality
        $query = Download::whereExport(true)->orderByDesc("created_at");

        // Apply search filter if search query exists
        if (!empty($search)) {
            $query->where(function($q) use ($search) {
                $q->where('name', 'like', '%' . $search . '%');
                // Add more search conditions if needed
            });
        }

        // Paginate the results
        $downloads = $query->paginate(15);

        // Pass the downloads to the view
        return view("version2.admin.exports", compact("downloads"));
    }
}
