<?php

namespace App\Http\Controllers;

use App\Attendee;
use App\Event;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class EventsController extends Controller
{
    public function index(){
        $events = Event::orderByDesc("created_at")->paginate(15);
        return view("version2.admin.events",compact("events"));
    }

    public function register(){
        $event = [];
        return view("version2.admin.register_event",compact("event"));
    }

    public function postRegister(Request $request) {

        $id = Uuid::generate()->string;

        $event = \App\Event::create([
            "id" => $id,
            "name" => $request->name,
            "description" => $request->description,
            "event_date" => $request->event_date,
            "deadline_date" => $request->deadline_date,
            "event_fee" => $request->event_fee,
            "member_assessment_fee" => $request->member_assessment_fee,
            "non_member_assessment_fee" => $request->non_member_assessment_fee,
            "participants" => $request->participants,
            "banner_image" => $request->banner_image,
            "location_name" => $request->location_name,
            "location_coordinates" => $request->location_coordinates,
            "contact_number" => $request->contact_number,
        ]);

        if ($request->hasFile("banner_image")){
            $imageName = time().'.'.$request->banner_image->extension();
            $request->banner_image->move(public_path('images/slides'), $imageName);
            \App\Event::find($id)->update(["banner_image" => $imageName]);
        }

        return redirect()->to("/version2/events");
    }

    public function getAttendees($id) {
        $attendees = Attendee::whereEventId($id)
            ->orderByDesc("created_at")->paginate(15);

        return view("version2.admin.attendees",compact("attendees"));

    }

    public function archiveEvent($id) {
        Event::find($id)->update(["archived" => true]);
        return redirect()->back()->with("Successfully archived event!");
    }

    public function unArchiveEvent($id) {
        Event::find($id)->update(["archived" => false]);
        return redirect()->back()->with("Successfully unarchived event!");
    }
}
