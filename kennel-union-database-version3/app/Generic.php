<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generic extends Model
{
    //

    protected $table = "generics";

    protected $guarded = [];

    protected $casts = [ "data_one" => "array","data_two" => "array"];
}
